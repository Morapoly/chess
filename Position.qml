import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

Rectangle {
    property int index
    property int dimention: 50
    property string horizontalPosition
    property string verticalPosition
    property string color1: "gray"
    property string color2: "darkGray"
    property alias visibleName: label.visible
    Label {
        id: label
        anchors.centerIn: parent
        text: horizontalPosition + verticalPosition
        visible: false
    }

    width: dimention
    height: dimention
    Component.onCompleted: {
        let residual = 0
        if (index % 16 >= 8)
            residual = 1
        if (index % 2 === residual)
            color = color1
        else
            color = color2
        switch (index % 8) {
            case 0:
                horizontalPosition = 'a'
                break
            case 1:
                horizontalPosition = 'b'
                break
            case 2:
                horizontalPosition = 'c'
                break
            case 3:
                horizontalPosition = 'd'
                break
            case 4:
                horizontalPosition = 'e'
                break
            case 5:
                horizontalPosition = 'f'
                break
            case 6:
                horizontalPosition = 'g'
                break
            case 7:
                horizontalPosition = 'h'
                break
        }
        switch (parseInt(index / 8)) {
            case 0:
                verticalPosition = '8'
                break
            case 1:
                verticalPosition = '7'
                break
            case 2:
                verticalPosition = '6'
                break
            case 3:
                verticalPosition = '5'
                break
            case 4:
                verticalPosition = '4'
                break
            case 5:
                verticalPosition = '3'
                break
            case 6:
                verticalPosition = '2'
                break
            case 7:
                verticalPosition = '1'
                break
        }
    }
}
