#include "MoveManager.h"
#include "Piece.h"

static auto instance = MoveManager::getInstance();

MoveManager::MoveManager()
{
    for (char h = 'a'; h <= 'h'; h++) {
        for (char v = '1'; v <= '8'; v++) {
            positionToPieces[QString("%1%2").arg(h).arg(v)] = std::nullopt;
        }
    }
}

void MoveManager::registerPiece(Piece* piece)
{
    positionToPieces[piece->getPosition().toString()] = piece;
    connect(piece, &Piece::positionChanged, this, [this](){
        auto piece = qobject_cast<Piece*>(sender());
        positionToPieces[piece->getPosition().toString()] = piece;
    }, Qt::QueuedConnection);
}

QStringList MoveManager::getPossibleMoves(const QString& currentPosition) const
{
    QStringList list;
    if (hasPiece(currentPosition)) {
        auto selectedPiece = positionToPieces[currentPosition];
//        auto k = selectedPiece;
    }
    return list;
}

bool MoveManager::hasPiece(const QString& position) const
{
    return positionToPieces[position].has_value();
}

Piece* MoveManager::getPiece(const QString& position) const
{
    return *positionToPieces[position];
}
