#pragma once

#include "Singleton.h"
#include <QHash>
#include <QString>
#include "PieceType.h"
#include <QObject>
#include "Piece.h"

class PieceAsset : public QObject, public Singleton<PieceAsset>
{
    Q_OBJECT
    friend class Singleton<PieceAsset>;
    PieceAsset();
public:
    void registerTypeName(PieceType type, const QString& name);
    void registerPiece(Piece* piece);
    PieceType toType(const QString& name) const;
    QString toName(PieceType type) const;
private:
    QHash<PieceType, QString> typeToNames;
    QHash<QString, PieceType> nameToTypes;
    QHash<PieceType, Piece*> typeToPieces;
    Piece::MoveRuler pawnRuler;
    void updatePieceMoveRuler(Piece* piece);
};

