import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import Piece 1.0

MenuItem {
    id: piece
    property int originX: 0
    property int originY: 0
    property int pieceDimention
    property string colorName: "white"
    readonly property int white: 0
    readonly property int black: 1
    property int color: colorName === "white" ? white : black
    property string name
    property string horizontalPosition
    property string verticalPosition
    width: pieceDimention
    height: pieceDimention
    y: originY
    spacing: 0
    padding: 0
    topPadding: 0
    bottomPadding: 0
    leftPadding: 0
    rightPadding: 0
    icon.source: color === white ? "white" + name + ".png" : "black" + name + ".png"
    icon.color: "transparent"
    icon.width: pieceDimention
    icon.height: pieceDimention
    onHorizontalPositionChanged: x = originX + (horizontalPosition.charCodeAt(0) - ('a').charCodeAt(0)) * pieceDimention
    onVerticalPositionChanged: y = originY + (8 - parseInt(verticalPosition)) * pieceDimention
    onClicked: callback.log()
    function getPosition() {
        return horizontalPosition + verticalPosition
    }
    PieceCallbacks {
        id: callback
        name: piece.name
        color: piece.color
        horizontalPosition: piece.horizontalPosition
        verticalPosition: piece.verticalPosition
    }
}
