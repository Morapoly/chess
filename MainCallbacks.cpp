#include "MainCallbacks.h"
#include <QDebug>
#include <QVariant>
#include <QJsonObject>

MainCallbacks::MainCallbacks(QObject *mainObj) :
    mainObj(mainObj)
{
    board = mainObj->findChild<QObject*>("board");
    addPieces();
}

void MainCallbacks::addPieces()
{
    QString color = "white";
    addPieces(color);
    color = "black";
    addPieces(color);
}

void MainCallbacks::addPieces(const QString& color)
{
    addPawns(color);
    addRooks(color);
    addKnights(color);
    addBishops(color);
    addQueens(color);
    addKings(color);
}

void MainCallbacks::addPawns(const QString& color)
{
    char horizontalPos = 'a';
    char vertivalPos = color == "white" ? '2' : '7';
    for (int i = 0; i < 8; i++) {
        QMetaObject::invokeMethod(board,
                                  "addPiece",
                                  Qt::QueuedConnection,
                                  Q_ARG(QString, "Pawn"),
                                  Q_ARG(QString, color),
                                  Q_ARG(QString, QString("%1%2").arg(horizontalPos).arg(vertivalPos)));
        horizontalPos++;
    }
}

void MainCallbacks::addRooks(const QString& color)
{
    char horizontalPos = 'a';
    char vertivalPos = color == "white" ? '1' : '8';
    for (int i = 0; i < 2; i++) {
        horizontalPos += i * 7;
        QMetaObject::invokeMethod(board,
                                  "addPiece",
                                  Qt::QueuedConnection,
                                  Q_ARG(QString, "Rook"),
                                  Q_ARG(QString, color),
                                  Q_ARG(QString, QString("%1%2").arg(horizontalPos).arg(vertivalPos)));
    }
}

void MainCallbacks::addKnights(const QString& color)
{
    char horizontalPos = 'b';
    char vertivalPos = color == "white" ? '1' : '8';
    for (int i = 0; i < 2; i++) {
        horizontalPos += i * 5;
        QMetaObject::invokeMethod(board,
                                  "addPiece",
                                  Qt::QueuedConnection,
                                  Q_ARG(QString, "Knight"),
                                  Q_ARG(QString, color),
                                  Q_ARG(QString, QString("%1%2").arg(horizontalPos).arg(vertivalPos)));
    }
}

void MainCallbacks::addBishops(const QString& color)
{
    char horizontalPos = 'c';
    char vertivalPos = color == "white" ? '1' : '8';
    for (int i = 0; i < 2; i++) {
        horizontalPos += i * 3;
        QMetaObject::invokeMethod(board,
                                  "addPiece",
                                  Qt::QueuedConnection,
                                  Q_ARG(QString, "Bishop"),
                                  Q_ARG(QString, color),
                                  Q_ARG(QString, QString("%1%2").arg(horizontalPos).arg(vertivalPos)));
    }
}

void MainCallbacks::addQueens(const QString& color)
{
    char horizontalPos = 'd';
    char vertivalPos = color == "white" ? '1' : '8';
    QMetaObject::invokeMethod(board,
                              "addPiece",
                              Qt::QueuedConnection,
                              Q_ARG(QString, "Queen"),
                              Q_ARG(QString, color),
                              Q_ARG(QString, QString("%1%2").arg(horizontalPos).arg(vertivalPos)));
}

void MainCallbacks::addKings(const QString& color)
{
    char horizontalPos = 'e';
    char vertivalPos = color == "white" ? '1' : '8';
    QMetaObject::invokeMethod(board,
                              "addPiece",
                              Qt::QueuedConnection,
                              Q_ARG(QString, "King"),
                              Q_ARG(QString, color),
                              Q_ARG(QString, QString("%1%2").arg(horizontalPos).arg(vertivalPos)));
}
