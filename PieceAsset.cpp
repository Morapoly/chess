#include "PieceAsset.h"
#include "MoveManager.h"
#include <QDebug>

static auto instance = PieceAsset::getInstance();

PieceAsset::PieceAsset() :
    pawnRuler([](const PiecePosition& currentPosition){
        QList<PiecePosition> list;
        auto manager = MoveManager::getInstance();
        if (!manager->hasPiece(currentPosition))
            return list;
        auto currentPiece = manager->getPiece(currentPosition);
        auto currentColor = currentPiece->getColor();
        int verticalDiff = currentColor == Piece::white ? +1 : -1;
        QList<PiecePosition> abstractMoves = {PiecePosition(currentPosition.getHorizontal(), currentPosition.getVertical(verticalDiff)),
                                              PiecePosition(currentPosition.getHorizontal(-1), currentPosition.getVertical(verticalDiff)),
                                              PiecePosition(currentPosition.getHorizontal(+1), currentPosition.getVertical(verticalDiff))};
        if (!manager->hasPiece(abstractMoves[0]))
            list.append(abstractMoves[0]);
        for (int index = 1; index <= 2; index++) {
            if (manager->hasPiece(abstractMoves[index])) {
                auto affectedPiece = manager->getPiece(abstractMoves[index]);
                if (affectedPiece->getColor() != currentColor) {
                    /// Avoid to mate king
                    list.append(abstractMoves[index]);
                }
            }
        }
        return list;
    })
{
    registerTypeName(pawn, "Pawn");
    registerTypeName(rook, "Rook");
    registerTypeName(knight, "Knight");
    registerTypeName(bishop, "Bishop");
    registerTypeName(queen, "Queen");
    registerTypeName(king, "King");
}

void PieceAsset::registerTypeName(PieceType type, const QString& name)
{
    typeToNames[type] = name;
    nameToTypes[name] = type;
}

void PieceAsset::registerPiece(Piece* piece)
{
    auto type = piece->getType();
    typeToPieces[type] = piece;
    updatePieceMoveRuler(piece);
    MoveManager::getInstance()->registerPiece(piece);
}

PieceType PieceAsset::toType(const QString& name) const
{
    return nameToTypes[name];
}

QString PieceAsset::toName(PieceType type) const
{
    return typeToNames[type];
}

void PieceAsset::updatePieceMoveRuler(Piece* piece)
{
    switch (piece->getType()) {
        case pawn:
            piece->setMoveRuler(pawnRuler);
            break;
        case rook:
            break;
        case knight:
            break;
        case bishop:
            break;
        case queen:
            break;
        case king:
            break;
    }
}
