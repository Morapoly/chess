#pragma once

#include <QString>

class PiecePosition
{
public:
    enum Horizontal {
        a, b, c, d, e, f, g, h
    };
    enum Vertical {
        _1, _2, _3, _4, _5, _6, _7, _8
    };
    PiecePosition(Horizontal horizontal, Vertical vertical);
    PiecePosition();
    Horizontal getHorizontal() const;
    void setHorizontal(Horizontal newHorizontal);
    void setHorizontal(const QString& newHorizontal);
    Vertical getVertical() const;
    void setVertical(Vertical newVertical);
    void setVertical(const QString& newVertical);
    QString toString() const;
    Horizontal getHorizontal(int diff) const;
    Vertical getVertical(int diff) const;
    operator QString() const;
private:
    Horizontal horizontal;
    Vertical vertical;
};

