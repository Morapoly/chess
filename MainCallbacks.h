#pragma once

#include <QObject>

class MainCallbacks : public QObject
{
    Q_OBJECT
public:
    explicit MainCallbacks(QObject *mainObj);
private:
    QObject* mainObj;
    QObject* board;
    void addPieces();
    void addPieces(const QString& color);
    void addPawns(const QString& color);
    void addRooks(const QString& color);
    void addKnights(const QString& color);
    void addBishops(const QString& color);
    void addQueens(const QString& color);
    void addKings(const QString& color);
};
