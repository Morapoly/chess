#include "Piece.h"
#include "PieceAsset.h"
#include <QDebug>
#include "PieceAsset.h"

Piece::Piece() :
    Piece(pawn)
{}

Piece::Piece(PieceType type, const PiecePosition& position) :
    type(type),
    position(position)
{
    updateName();
    updateType();
    doConnections();
}

PieceType Piece::getType() const
{
    return type;
}

const QString& Piece::getName() const
{
    return name;
}

const PiecePosition& Piece::getPosition() const
{
    return position;
}

Piece::Color Piece::getColor() const
{
    return color;
}

void Piece::setPosition(const PiecePosition& newPosition)
{
    position = newPosition;
}

void Piece::log()
{
    auto s = moveRuler(position);
    for (auto l : s)
        qDebug() << l;
}

void Piece::setType(PieceType newType)
{
    type = newType;
    updateName();
}

void Piece::updateName()
{
    name = PieceAsset::getInstance()->toName(type);
}

void Piece::updateType()
{
    auto asset = PieceAsset::getInstance();
    type = asset->toType(name);
    asset->registerPiece(this);
    emit typeChanged();
}

void Piece::doConnections()
{
    connect(this, &Piece::nameChanged, this, [this](){
        updateType();
    }, Qt::QueuedConnection);
    connect(this, &Piece::colorChanged, this, [this](){
        color = static_cast<Color>(colorNumber);
    }, Qt::QueuedConnection);
    connect(this, &Piece::horizontalPositionChanged, this, [this](){
        position.setHorizontal(horizontalPosition);
        emit positionChanged();
    }, Qt::QueuedConnection);
    connect(this, &Piece::verticalPositionChanged, this, [this](){
        position.setVertical(verticalPosition);
        emit positionChanged();
    }, Qt::QueuedConnection);
}

void Piece::setMoveRuler(const MoveRuler& moveRuler)
{
    this->moveRuler = moveRuler;
}
