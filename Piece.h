#pragma once

#include <QString>
#include "PieceType.h"
#include "PiecePosition.h"
#include <QObject>
#include <memory>
#include <functional>

class Piece : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name MEMBER name NOTIFY nameChanged)
    Q_PROPERTY(int color MEMBER colorNumber NOTIFY colorChanged)
    Q_PROPERTY(QString horizontalPosition MEMBER horizontalPosition NOTIFY horizontalPositionChanged)
    Q_PROPERTY(QString verticalPosition MEMBER verticalPosition NOTIFY verticalPositionChanged)
    using MoveRuler = std::function<QList<PiecePosition>(const PiecePosition& currentPosition)>;
    friend class PieceAsset;
public:
    enum Color {
        white = 0,
        black = 1
    };
    explicit Piece();
    explicit Piece(PieceType type, const PiecePosition& position = PiecePosition());
    PieceType getType() const;
    const QString& getName() const;
    const PiecePosition& getPosition() const;
    Color getColor() const;
    void setPosition(const PiecePosition& newPosition);
    Q_INVOKABLE void log();
private:
    PieceType type;
    QString name;
    int colorNumber;
    Color color;
    QString horizontalPosition;
    QString verticalPosition;
    PiecePosition position;
    MoveRuler moveRuler;
    void setType(PieceType newType);
    void updateName();
    void updateType();
    void doConnections();
    void setMoveRuler(const MoveRuler& moveRuler);
signals:
    void typeChanged();
    void nameChanged();
    void colorChanged();
    void horizontalPositionChanged();
    void verticalPositionChanged();
    void positionChanged();
};
