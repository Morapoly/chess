#pragma once

#include "Singleton.h"
#include <QStringList>
#include <QHash>
#include <QObject>
#include <optional>

class Piece;
class MoveManager : public QObject, public Singleton<MoveManager>
{
    Q_OBJECT
    friend class Singleton<MoveManager>;
    MoveManager();
public:
    void registerPiece(Piece* piece);
    QStringList getPossibleMoves(const QString& currentPosition) const;
    bool hasPiece(const QString& position) const;
    Piece* getPiece(const QString& position) const;
private:
    QHash<QString, std::optional<Piece*>> positionToPieces;
};

