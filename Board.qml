import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

Item {
    id: board
    property int pieceDimention: 50
    property int dimention: 8 * pieceDimention
    width: dimention
    height: dimention
    Grid {
        id: grid
        rows: 8
        columns: 8
        rowSpacing: 0
        columnSpacing: 0
        Repeater {
            id: repeater
            property int index: 0
            model: 8 * 8
            Position {
                index: repeater.index
                dimention: board.pieceDimention
                Component.onCompleted: repeater.index++
            }
        }
    }
    function addPiece(name: string, color: string, position: string) {
        var component = Qt.createComponent("Piece.qml")
        var sprite = component.createObject(board, {name: name,
                                                    colorName: color,
                                                    pieceDimention: board.pieceDimention,
                                                    horizontalPosition: position[0],
                                                    verticalPosition: position[1]});
    }
}
