import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15

Window {
    width: 8 * 50
    height: 8 * 50
    visible: true
    title: qsTr("Chess")
    color: "lightGray"

    ColumnLayout {
        anchors.fill: parent
        Board {
            id: board
            pieceDimention: 50
            objectName: "board"
        }
    }
}
