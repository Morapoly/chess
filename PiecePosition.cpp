#include "PiecePosition.h"

PiecePosition::PiecePosition(Horizontal horizontal, Vertical vertical) :
    horizontal(horizontal),
    vertical(vertical)
{}

PiecePosition::PiecePosition() {}

PiecePosition::Horizontal PiecePosition::getHorizontal() const
{
    return horizontal;
}

void PiecePosition::setHorizontal(Horizontal newHorizontal)
{
    horizontal = newHorizontal;
}

void PiecePosition::setHorizontal(const QString& newHorizontal)
{
    horizontal = static_cast<Horizontal>(static_cast<int>(newHorizontal[0].toLatin1()) - static_cast<int>('a'));
}

PiecePosition::Vertical PiecePosition::getVertical() const
{
    return vertical;
}

void PiecePosition::setVertical(Vertical newVertical)
{
    vertical = newVertical;
}

void PiecePosition::setVertical(const QString& newVertical)
{
    vertical = static_cast<Vertical>(static_cast<int>(newVertical[0].toLatin1()) - static_cast<int>('1'));
}

QString PiecePosition::toString() const
{
    QString str;
    switch (horizontal) {
        case a:
            str += 'a';
            break;
        case b:
            str += 'b';
            break;
        case c:
            str += 'c';
            break;
        case d:
            str += 'd';
            break;
        case e:
            str += 'e';
            break;
        case f:
            str += 'f';
            break;
        case g:
            str += 'g';
            break;
        case h:
            str += 'h';
            break;
    }
    switch (vertical) {
        case _1:
            str += '1';
            break;
        case _2:
            str += '2';
            break;
        case _3:
            str += '3';
            break;
        case _4:
            str += '4';
            break;
        case _5:
            str += '5';
            break;
        case _6:
            str += '6';
            break;
        case _7:
            str += '7';
            break;
        case _8:
            str += '8';
            break;
    }
    return str;
}

PiecePosition::Horizontal PiecePosition::getHorizontal(int diff) const
{
    return static_cast<Horizontal>(static_cast<int>(horizontal) + diff);
}

PiecePosition::Vertical PiecePosition::getVertical(int diff) const
{
    return static_cast<Vertical>(static_cast<int>(vertical) + diff);
}

PiecePosition::operator QString() const
{
    return toString();
}
