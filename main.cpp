#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "MainCallbacks.h"
#include <QLocale>
#include <QTranslator>
#include "Piece.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "Chess_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    qmlRegisterType<Piece>("Piece", 1, 0, "PieceCallbacks");

    QQmlApplicationEngine engine;
    MainCallbacks* callbacks;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url, &callbacks](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
        else {
            callbacks = new MainCallbacks(obj);
        }
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
